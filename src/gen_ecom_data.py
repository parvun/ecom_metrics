import pandas as pd
import requests
import configparser
import datetime
import json
import sys
import os
import mysql.connector as sql
from openpyxl.utils.dataframe import dataframe_to_rows
from openpyxl import load_workbook


def define_input(date_limits, max_rows, max_bare_rows, tblname):

    """
    :param date_limits: The timestamp start and end for daily report
    :param max_rows:  Maximum lines returned
    :param max_bare_rows: Maximum lines fetched
    :param tblname: Source Table of information
    :return: Query
    """

    input = {}
    input['database'] = 5
    input['type'] = "query"
    input['query'] = {}
    input['parameters'] = []

    if tblname == "entry_approval_request":
        input['query']['source_table'] = 425
        input['query']['filter'] = [["BETWEEN", ["field-id", 4761], str(date_limits[0]), str(date_limits[1])]]
    elif tblname == "ecom_partner_settings":
        input['query']['source_table'] = 425
    else:
        print("Table name not valid")
        sys.exit(1)

    input['constraints'] = {"max-results": max_rows, "max-results-bare-rows": max_bare_rows}
    return input


def calculate_epoch_time(in_date=""):

    """
    :param in_date: Input date for metrics collection, if empty , today's date
    :return: epoch timestamp in milliseconds from start of day to end of day
    """

    if in_date == "":
        inpdate = datetime.datetime.today()
    else:
        inpdate = datetime.datetime.strptime(in_date, '%Y%m%d')

    nextday = inpdate + datetime.timedelta(days=1)

    starttime = datetime.datetime(inpdate.year, inpdate.month, inpdate.day, 12, 0).timestamp() * 1000
    endtime = datetime.datetime(nextday.year, nextday.month, nextday.day, 12, 0).timestamp() * 1000

    return starttime, endtime


def get_raw_data(in_url, in_query, outfile):
    """

    :param in_url: The API endpoint
    :param in_query: Query
    :param outfile: Output CSV Data File
    :return: None
    """
    # POST Method
    try:
        res = requests.post(in_url, data=json.dumps(in_query))
    except Exception as e:
        print(e)

    if res.text:
        try:
            response = json.loads(res.text)
            f = open(outfile, "w+")
            f.write(response.text)
            f.close()
        except Exception as e:
            print(e)


def load_request_data(outfile, dim_partner):
    """

    :param outfile: CSV Data File, data/entry_approval_request.csv
    :param partner_file: partner_info.csv containing Partner Information
    :return: Request Info Data Frame, ecom_report
    """
    ecom_data = pd.read_csv(outfile)
    ecom_report = ecom_data.merge(dim_partner[['partner_id', 'partner_name']], left_on='partner_id',
                                  right_on='partner_id', how='left')
    ecom_report = ecom_report[selected_columns.split(',')]

    # ecom_report.to_csv(outfile_selected_data, index=None, header=True)
    return ecom_report


def load_settings_data(outfile, dim_partner):
    """

    :param outfile: CSV Data File, data/ecom_partner_settings.csv
    :param partner_file: partner_info.csv containing Partner Information
    :return: Settings Info Data Frame, settings_report
    """
    settings_data = pd.read_csv(outfile)
    settings_data = settings_data['entity_type'] == 'SOCIETY'
    settings_data = settings_data['enabled'] == 'true'

    settings_report = settings_data.merge(dim_partner[['partner_id', 'partner_name']], left_on='partner_id',
                                          right_on='partner_id', how='left')
    settings_report = settings_report[selected_columns.split(',')]

    # city_report.to_csv(outfile_selected_data, index=None, header=True)
    return settings_report


def load_mysql_data(in_dataframe, dim_partner):
    """

    :param in_dataframe: settings_dataframe containing society--partner information from mongo
    :return: Dataframe, city datframe containg partner-society-city information
    """

    db_connection = sql.connect(host='mygate-gamma-20may19.c8tai7cxbkxx.ap-south-1.rds.amazonaws.com', database='mygate', user='mygate', password='mygategamma123#')
    city_data = pd.read_sql('select distinct a.id as society_id, \
        a.city_id as city_id, \
        b.cnd_name as city \
        from mg_society a \
        left join mg_cnd b \
        on a.city_id = b.id', con=db_connection)

    city_data = in_dataframe.merge(city_data[['society_id', 'city_id', 'city']], left_on='entity_id',
                                   right_on='society_id', how='left')

    city_data = city_data.merge(dim_partner[['partner_id', 'partner_name']], left_on='partner_id',
                                right_on='partner_id', how='left')

    city_data = city_data[['_id','partner_id','partner_name', 'society_id','city_id','city']]
    return city_data


def generate_report(template, request_data, city_data, final_report):

    """

    :param template: Input Excel Template
    :param request_data: Dataframe with data from entry_approval_request
    :param city_data:  Datafame with partner-society-city info
    :param final_report: Final Excel Report
    :return: None
    """
    wb = load_workbook(template)

    wb.create_sheet('request raw data')
    wb.create_sheet('society raw data')

    ws2 = wb['request raw data']
    ws3 = wb['society raw data']

    for row in dataframe_to_rows(request_data, index=False, header=True):
        ws2.append(row)

    for row in dataframe_to_rows(city_data, index=False, header=True):
        ws3.append(row)

    wb.save(final_report)


if __name__ == "__main__":

    dir_path = os.path.dirname(os.path.realpath(__file__))

    # Read Configuration Parameters
    config = configparser.ConfigParser()
    config.read(dir_path + '/config_properties.ini')

    indate = config['INPUT']['InDate']
    max_results = config['RESULT']['max-results']
    max_results_bare_rows = config['RESULT']['max-results-bare-rows']
    url = config['ENDPOINT']['Host']
    request_outfile_all_data = dir_path + '/../data/' + config['OUTPUT']['RequestOutFile_AllData']
    settings_outfile_all_data = dir_path + '/../data/' + config['OUTPUT']['SettingsOutFile_AllData']
    selected_columns = config['OUTPUT']['Selected_Columns']
    partner_info = dir_path + '/../data/' + config['PARTNER']['partner_info']
    template = dir_path + '../data/' + 'ecom_metrics_template.xlsx'
    final_report = dir_path + '../data/' + 'EcomPartner_Daily_Report.xlsx'

    if max_results == "" or max_results_bare_rows == "" or url == "" or request_outfile_all_data == "" or selected_columns == "" \
        or partner_info == "":
        print("[ERROR] : Please provide all required info in config_properties.ini")
        sys.exit(1)

    # Calculate Timestamp
    time_interval = calculate_epoch_time(indate)
    if time_interval[0] == "" or time_interval[1] == "":
        print("[ERROR] : Time interval not calculated, Please verify the Input Date")
        sys.exit(1)

    # Generate Query For Request Data
    request_query = define_input(time_interval, max_results, max_results_bare_rows, "entry_approval_request")

    # Generate raw Data
    get_raw_data(url, request_query, request_outfile_all_data)

    # Load Partner Info
    dim_partner = pd.read_csv(partner_info)

    if os.stat(request_outfile_all_data).st_size != 0:
        # Generate Request df
        request_dataframe = load_request_data(request_outfile_all_data, dim_partner)
    else:
        print("[ERROR] : " + request_outfile_all_data + " not generated")
        sys.exit(1)

    # Generate Query For Partner Settings Data
    settings_query = define_input(time_interval, max_results, max_results_bare_rows, "ecom_partner_settings")

    # Generate raw Data
    get_raw_data(url, settings_query, settings_outfile_all_data)
    if os.stat(request_outfile_all_data).st_size != 0:
        # Generate Settings df
        settings_dataframe = load_settings_data(settings_outfile_all_data, dim_partner)
    else:
        print("[ERROR] : " + settings_outfile_all_data + " not generated")
        sys.exit(1)

    # Generate City Data
    city_dataframe = load_mysql_data(settings_dataframe,dim_partner)

    # Generate Final Report
    generate_report(template, request_dataframe, city_dataframe, final_report)

